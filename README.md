# ZAMRO

* Install mariadb via ansible
* Create jenkins job automatically via JobDSL

## Run instructions

1. `vagrant up` spins up CentOS 7, installs dependecnies and launces mariadb + smoke test. In addition, empty Jenkins instance is spinned up available on port 8000
2. Login to Jenkins with `admin`/`jenkins` credentials. Go to plugin manager and install Git (3.9.1) and JobDSL plugins.
3. Go to Manage Jenkins > Configure System and set username and email for Git plugin to `jenkins` and `jenkins@localhost`
3. Create seed job from `roles/jenkins/files/job.dsl` and run it.