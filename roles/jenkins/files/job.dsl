def gitUrl = 'https://github.com/beerkeeper/python-ip-script.git'
job('generated') {
    scm {
        git(gitUrl)
    }
    steps {
        shell('pip install -r requirements.txt')
        shell('python main.py')
    }
    queue('generated')
}